﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SQLite;

namespace DataBaseCreator
{
    [Table("Tasks")]
    public class TaskToDo
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public DateTime DateTime { get; set; }
        public bool IsDone { get; set; } = false;

        public override string ToString()
        {
            return string.Format("[Task: ID={0}, Name={1}, Description={2}]", ID, Name, Description);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var db = new SQLiteConnection("D:\\Praktyki\\ToDo\\DataBase\\data.sqllite");
            //db.CreateTable<TaskToDo>();
            //db.Insert(new TaskToDo
            //{
            //    Name = "Makaron",
            //    DateTime = DateTime.Now,
            //    Description = "NICNICNIC"
            //});




           // CreateDatabase(@"D:\Praktyki\ToDo\DataBase");
            //CreateDatabase(@"..\..\ToDo\DataBase").Wait();
        }
        private static async Task<string> CreateDatabase(string path)
        {
            try
            {
                var connection = new SQLiteAsyncConnection(path);

                await connection.CreateTableAsync<TaskToDo>();

                return "Database created";
            }
            catch (SQLiteException ex)
            {
                return "ERROR";
            }
        }
    }
}
