﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using SQLitePCL;
using Xamarin.Forms;
using System.IO;
using ToDo.Droid;
using ToDo.Models;

[assembly: Dependency(typeof(SQLiteAndroid))]
namespace ToDo.Models
{
    public class SQLiteAndroid : ISqlLite
    {
        public SQLiteConnection GetConnection()
        {
            var fileName = "dataBase.db3";
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, fileName);

            var connection = new SQLiteConnection(path);

            return connection;
        }

    }
}
