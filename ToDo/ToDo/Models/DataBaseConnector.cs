﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace ToDo.Models
{
    public interface ISqlLite
    {
        SQLiteConnection GetConnection();
    }

    public class DataBaseConnector
    {
        private SQLiteConnection _connection;

        public DataBaseConnector()
        {
            _connection = DependencyService.Get<ISqlLite>().GetConnection();
            _connection.CreateTable<TaskToDo>();
        }

        public void AddTask(TaskToDo task)
        {
            
            _connection.Insert(task);
        }
        public IEnumerable<TaskToDo> GetTasks()
        {
            return _connection.Table<TaskToDo>();
        }
        public IEnumerable<string> GetTasksNames()
        {
            return _connection.Table<TaskToDo>().Select(x => x.Name).ToList();
        }

    }
}