﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
namespace ToDo.Models
{
    public class TaskToDo
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public DateTime DateTime { get; set; }
        public bool IsDone { get; set; } = false;

        
    }
}
