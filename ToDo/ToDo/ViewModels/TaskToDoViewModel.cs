﻿using System;
using System.Linq;
using ToDo.Models;

namespace ToDo.ViewModels
{
    public class TaskToDoViewModel
    {
        private DataBaseConnector dataBaseConnector = new DataBaseConnector();

        public void AddTask(string name, DateTime dateTime, string description)
        {
            dataBaseConnector.AddTask(
                new TaskToDo
                {
                    Name = name,
                    DateTime = dateTime,
                    Description = description
                });
        }

        public TaskToDo GetTaskByName(string taskName)
        {
            return dataBaseConnector.GetTasks().FirstOrDefault(x=>x.Name==taskName);
        }

    }
}